# Модель события
class Event < ActiveRecord::Base
  # событие всегда принадлежит юзеру
  belongs_to :user

  # у события много комментариев и подписок
  has_many :comments, dependent: :destroy
  has_many :subscriptions, dependent: :destroy

  # у события много фотографий
  has_many :photos, dependent: :destroy

  # у события много подписчиков (объекты User), через таблицу subscriptions, по ключу user_id
  has_many :subscribers, through: :subscriptions, source: :user


  # юзера не может не быть
  validates :user, presence: true

  # категория не может не быть
  validates :category, presence: true

  # заголовок должен быть, и не длиннее 255 букв
  validates :title, presence: true, length: {maximum: 255}

  validates :address, presence: true
  validates :datetime, presence: true

  # Метод, который возвращает всех, кто пойдет на событие:
  # всех подписавшихся и организатора
  def visitors
    (subscribers + [user]).uniq
  end

  CATEGORIES = {
    0 => "Компьютеры и сетевое оборудование",
    1 => "Телефоны и телекоммуникации",
    2 => "Одежда и аксессуары",
    3 => "Бытовая электроника",
    4 => "Багаж и сумки",
    5 => "Ювелирные изделия",
    6 => "Для дома и сада",
    7 => "Часы",
    8 => "Другие категории"
  }

end
